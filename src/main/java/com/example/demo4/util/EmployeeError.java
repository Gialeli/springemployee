package com.example.demo4.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class EmployeeError extends Exception{
	
	@Getter private String errorMessage;
	
	
}
