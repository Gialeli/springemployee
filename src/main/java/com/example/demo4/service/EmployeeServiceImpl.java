package com.example.demo4.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo4.model.Employee;

import lombok.extern.slf4j.Slf4j;

@Service("employeeService")
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

	private static List<Employee> employees;
	
	static {
		employees = populateDummyUsers();
	}

	@Override
	public List<Employee> findAllEmployees() {
		return employees;
	}
	
	private static List<Employee> populateDummyUsers(){
		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(1, "Vasilki"));
		employees.add(new Employee(2, "Stauroula"));
		employees.add(new Employee(3, "Fotini"));
		employees.add(new Employee(4, "Maria"));
		employees.add(new Employee(5, "kwnstantina"));
		return employees;
	}

	@Override
	public Employee findById(int id) {
		return employees.get(id);
	}

	@Override
	public Employee findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteEmployeeById(int id) {
		employees.remove(id);
		
	}

	@Override
	public void deleteAllEmployees() {
		
	}

	@Override
	public boolean isEmployeeExists(Employee employee) {
		// TODO Auto-generated method stub
		return false;
	}
}
