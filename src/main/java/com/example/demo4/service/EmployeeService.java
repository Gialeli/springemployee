package com.example.demo4.service;

import java.util.List;

import com.example.demo4.model.Employee;


public interface EmployeeService {
	
	List<Employee> findAllEmployees();
	
	Employee findById(int id);
	
	Employee findByName(String name);
	void saveEmployee(Employee employee);
	void updateEmployee(Employee employee);
	void deleteEmployeeById(int id);
	
	void deleteAllEmployees();
	boolean isEmployeeExists(Employee employee);
	

}
