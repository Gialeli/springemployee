package com.example.demo4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.model.Employee;
import com.example.demo4.service.EmployeeService;
import com.example.demo4.util.EmployeeError;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping(value ="/api")
public class RestApiController {
	
	@Autowired
	EmployeeService employeeService;

	/**
	 * This is for testing
	 * @return a String for test http response
	 */
	/*@RequestMapping(value="/testme", method = RequestMethod.GET)
	public ResponseEntity<String> testme(){
		String myreturn="hello";
		return new ResponseEntity<String>(myreturn, HttpStatus.OK);
	}*/
	
	@GetMapping(value = "/testme")
	public ResponseEntity<String> testme() {
		log.debug("Entering {}", "testme");
		String myreturn = "hello";
		log.debug("leaving {}", "testme");
		return new ResponseEntity<String>(myreturn, HttpStatus.OK);
	}
	
	/**
	 * retrieve all employees
	 * @return
	 */
	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> listAllUsers() {
		List<Employee> employees = employeeService.findAllEmployees();
		if (employees.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);

		}
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}
	
	/**
	 * Delete an employee
	 * @return
	 */
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
	   public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
	       log.info("Fetching & Deleting Employee with id {}", id);

	       Employee employee = employeeService.findById(id);
	       if (employee == null) {
	           log.error("Unable to delete. Employee with id {} not found.", id);
	           return new ResponseEntity(
	                   new EmployeeError("Unable to delete. Employee with id " + id + " not found."),
	                   HttpStatus.NOT_FOUND);
	       }
	       employeeService.deleteEmployeeById(id);
	       return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
	   }
	
	/**
	 * retrieve Employee
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	   public ResponseEntity<?> getUser(@PathVariable("id") int id) {
	       log.info("Fetching & Deleting Employee with id {}", id);

	       Employee employee = employeeService.findById(id);
	       if (employee == null) {
	           log.error("Employee with id {} not found.", id);
	           return new ResponseEntity(
	                   new EmployeeError("Employee with id " + id + " not found."),
	                   HttpStatus.NOT_FOUND);
	       }
	       employeeService.deleteEmployeeById(id);
	       return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	   }
	
	
	
	
}
